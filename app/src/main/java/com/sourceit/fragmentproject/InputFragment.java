package com.sourceit.fragmentproject;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputFragment extends Fragment {

    public static final String EXTRA_TEXT = "extra_text";

    private static InputFragment instance;

    public static InputFragment newInstance(String text) {
        InputFragment fragment = new InputFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_TEXT, text);
        fragment.setArguments(args);
        return fragment;
    }

    public static InputFragment getInstance() {
        if (instance != null) {
            instance = new InputFragment();
        }
        return instance;
    }

    @BindView(R.id.text)
    TextView text;

    String welcome;
    Showable showable;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            welcome = getArguments().getString(EXTRA_TEXT, "");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            showable = (Showable) context;
        } else {
            throw new IllegalArgumentException(
                    context.getClass().getSimpleName() +
            " must implement Showable");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_input, container, false);
        ButterKnife.bind(this, root);


//        if (getActivity() instanceof MainActivity) {
//            ((MainActivity)getActivity()).show();
//        } else if (getActivity() instanceof PartActivity) {
//            ((PartActivity)getActivity()).show();
//        }

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        text.setText(welcome);
    }

    @OnClick(R.id.button)
    public void action() {
        showable.show();
    }
}
