package com.sourceit.fragmentproject;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements Showable, SampleFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, InputFragment.newInstance("Hello from activity"))
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void show() {
        Toast.makeText(this, "show", Toast.LENGTH_SHORT).show();


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, SampleFragment.newInstance())
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
