package com.sourceit.fragmentproject;

/**
 * Created by wenceslaus on 12.05.18.
 */

public interface Showable {
    void show();
}
