package com.sourceit.fragmentproject;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PartActivity extends AppCompatActivity {

    public static void start(Context context, String text) {
        Intent intent = new Intent(context, PartActivity.class);
        intent.putExtra(InputFragment.EXTRA_TEXT, text);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_part);
    }
}
